# Section 17: Building REST API's with Python and Django

## Overview

### 226. Introduction to API

Receive request and return data in many cases json files.

### 227. Building a basic REST API using Django REST Framework
Beware of typo error: serializer != serializers
```python
#models.py
from django.db import models
class Task1(models.Model):
	task_name = models.CharField(max_length=200)
	task_desc = models.CharField(max_length=200)
	date_created = models.DateTimeField(auto_now=True)  

#serializers.py
from .models import Task1
from rest_framework import serializers

class TaskSerializers(serializers.ModelSerializer):
	class Meta:
		model = Task1
		fields = ('id','task_name','task_desc')
#urls.py
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from restapp.views import TaskViewSet

router = routers.DefaultRouter()
router.register('task/', TaskViewSet)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
]
#settings.py
INSTALLED_APPS = [
    'rest_framework',
    'restapp.apps.RestappConfig',

#views.py 
from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TaskSerializers
from .models import Task1

class TaskViewSet(viewsets.ModelViewSet):
	queryset = Task1.objects.all().order_by('-date_created')
	serializer_class = TaskSerializers
```

### 228. Creating API Endpoints
```python 
class DueTaskViewSet(viewsets.ModelViewSet):
	queryset = Task1.objects.all().order_by('-date_created').filter(completed=False)
	serializer_class = TaskSerializers

class CompletedTaskViewSet(viewsets.ModelViewSet):
	queryset = Task1.objects.all().order_by('-date_created').filter(completed=True)
	serializer_class = TaskSerializers
```
### 229. Adding Image Field

```python
#install pillow
#in urls.py
from django.conf.urls.static import static
from django.conf import settings
urlspattern = [...]
+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#in serializers.py
image = serializers.ImageField(max_length=None, use_url=True)
	class Meta:
		model = Task1
		fields = ('id','task_name','task_desc','completed','date_created','image')
#in models.py add this field
image = models.ImageField(upload_to='Images/', default='Images/None/No0img.jpg')
#in settings.py
MEDIA_ROOT = os.path.join(BASE_DIR,'media')
MEDIA_URL = '/media/'
```

### 230. Filtering

```python
pip install django-filter #not filters
#fix some Imported errors bugs 
#in views.py 
from rest_framework import filters
class TaskViewSet(viewsets.ModelViewSet):
	queryset = Task1.objects.all().order_by('-date_created')
	serializer_class = TaskSerializers

	filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter)
	filter_fields = ('completed')
	ordering = ('-date_created',)
#in settings.py add to INSTALLED_APPS
	'django_filters',
#ulrs.py
	router = routers.DefaultRouter()
	router.register('task', TaskViewSet)
```

### 231. Search functionality

```python
filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
search_field = ('task_name')
```


### 232. API Authentication

```python
#urls.py
from restapp.views import TaskViewSet, CreateUserView
urlpatterns = [
	path('register', views.CreateUserView.as_view(), name='user'),
	path('api-auth', include('rest_framework.urls', namespace='rest_framework')),
]
#serializers.py 
from django.contrib.auth import get_user_model

class UserSerializer(serializers.ModelSerializer):
	password = serializers.CharField(write_only=True)
	def create(self, validated_data):
		user = get_user_model().objects.create(
				username = validated_data['username']
			)
		user.set_password(validated_data['password'])
		user.save()
		return user
	class Meta:
		model = get_user_model()
		fields = ('username', 'password')
#views.py 
from rest_framework import filters
from django_filters import rest_framework
from django_filters.rest_framework import DjangoFilterBackend
import django_filters.rest_framework
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth import get_user_model
from rest_framework.generics import CreateAPIView
class CreateUserView(CreateAPIView):
	model = get_user_model()
	permissions_classes = (AllowAny)
	serializer_class = UserSerializer
#settings.py
LOGIN_REDIRECT = '/task'
```


