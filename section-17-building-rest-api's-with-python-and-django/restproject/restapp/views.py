from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TaskSerializers, UserSerializer
from .models import Task1
from rest_framework import filters
from django_filters import rest_framework
from django_filters.rest_framework import DjangoFilterBackend
import django_filters.rest_framework
from rest_framework.permissions import IsAuthenticated, AllowAny
from django.contrib.auth import get_user_model
from rest_framework.generics import CreateAPIView
# Create your views here.

class TaskViewSet(viewsets.ModelViewSet):
	permissions_classes = (IsAuthenticated,)
	queryset = Task1.objects.all().order_by('-date_created')
	serializer_class = TaskSerializers

	filter_backends = (DjangoFilterBackend, filters.OrderingFilter, filters.SearchFilter)
	filter_fields = ('completed')
	ordering = ('-date_created')
	search_field = ('task_name')

class CreateUserView(CreateAPIView):
	model = get_user_model()
	permissions_classes = (AllowAny)
	serializer_class = UserSerializer

'''
class DueTaskViewSet(viewsets.ModelViewSet):

	queryset = Task1.objects.all().order_by('-date_created').filter(completed=False)
	serializer_class = TaskSerializers

class CompletedTaskViewSet(viewsets.ModelViewSet):
	queryset = Task1.objects.all().order_by('-date_created').filter(completed=True)
	serializer_class = TaskSerializers
	'''