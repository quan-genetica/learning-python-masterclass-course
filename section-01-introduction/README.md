# Section 1: Introduction

### Introduction to the Course

Will learn about:

- Python
- Tkinter(and build some small desktop applications)
- Django(and build a website with almost all the important parts)
- Flask
- Web Scrapping(how to crawl websites and build a real crawler)