# Section 03: Control Structures In Python

## Overview

### 13. If statement in Python

### 14. Elif statement in Python

### 15. Introduction to lists in Python

### 16. List operations in Python

### 17.List functions in Python

- List.append()
- List.len()
- list.insert(1, "Banana")
- list.index("item") 

### 18. Range function in Python

- num=list(range(10)) => a list from 0-9
- range(3,8) => 3,4,5,67,7
- range(1,10,3) => 1,4,7..

### 19. Code reuse and functions in Python

### 20. For loop in Python

- for x in range(0,20,2): print x => 0 2 4 ... 18

### 21. Boolean logic in Python

- if username == "a" and(or) password == "123": print("valid")

### 22. While loop in Python

- If condition in while statement already unmet, no command in loop will be executed.
-  Eg: This won't print anything:

- ```python
  counter = 1
  while(counter<10) and (counter>2):
      print(counter)
      counter+=1
  ```

### 23.Quiz For Section 2:

### 24. Coding challenge 2:

Pretty simple challenge:

```python
food = ['cauliflower', 'turmeric', 'pepperine', 'fish', 'broccoli']
print(food[2])
food.append('corn')
print(food)
food.insert(2, 'tacos')
print(food)
```

### 25. Coding challenge 2 solution

### 26. Coding challenge 3

```python
#Task 1: Using a for-loop and a range function, print "I am a programmer" 5 times.
for i in range(5):
    print('I am a programmer')
#Task 2: Create a function which displays out the square values of numbers from 1 to 9.
def ham1():
    for i in range(1,10):
        print(i*i)
ham1()
```

### 27. Notes & Summary For Section 2