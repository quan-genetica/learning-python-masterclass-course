# Section 2: Basic Python Concepts

## Overview

Very basic concepts

### 2. Installing Python And PyCharm

Check python version by go to Setting -> search Python Console

### 3. Hello World Program In Python

### 4. Some other mathematical operations in Python

- // means integer divive

- / means float divive

### 5. Strings In Python

### 6. Accepting input from the user in Python

Default type is string 

### 7. Performing operations on a string in Python

Need to + " " to add space between strings

### 8. Variables in Python

### 9. In place operators in Python

### 10. Writing our very first program in PyCharm

### 11. Coding Challenge Part 1

### 12. Notes & Summary For Section 1