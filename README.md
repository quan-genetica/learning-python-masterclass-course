# The Complete Python Masterclass: Learn Python From Scratch					
Journal for Learning course The Complete Python Masterclass: Learn Python From Scratch:https://www.udemy.com/course/python-masterclass-course/
## Planning

- Expected Time: 90 hours
- Finish day: 28/07/2020

## Day 0 (15/7/2020):

### Today Progress

Learned about basic Python concepts like:

- Math operations
- Input
- Types
- Variable
- In place operators

### Thoughts

Mostly the things I learned before except git flow task

## Day 1 (16/7/2020)

### Today's progress

Learn about:

- Control Structures:
  - If else, for, while loop.
  - List operations and function
  - Range function
  - Boolean logic

- Functions and Modules:
  - Parameter vs Arguments
  - Passing functions as arguments

- Exception and file handling:
  - Try, except, finally block.
  - Write, read, append.

### Thoughts

Still relatively easy to understand

### Links to work

[Section 1](section-01-introduction/README.md)

[Section 2](section-02-basic-python-concepts/README.md)

[Section 3](chapter-03-control-structures-in-python/README.md)

[Section 4](section-04-functions-&-modules-in-python/README.md)

## Day 2 (17/07/2020)

### Today's Progress

Learned about:

- Exception and file handling 
- Functional programming
- Types in python

### Links to work

[Section 5](section-05-exception-handling-&-file-handling-python/README.md)

[Section 6](section-06-some-more-types-in-python/README.md)

[Section 7](section-07-functional-programming-in-python/README.md)

## Day 3 (18/07/2020)

### Today's Progress

Learned about:

- Set
- OOP in Python
- Regular expressions

### Links to work

[Section 8](section-08-object-oriented-programming-in-python/README.md)

[Section 9](section-09-regular-expressions-in-python/README.md)

## Day 4 (19/07/2020)

### Today's Progress

Learned about how to:

- Build an GUI app using Tkinter
- Build an Calculator app Tkinter

### Links to work

[Section 10](section-10-create-gui-apps-in-python-using-tkinter/README.md)

[Section 11](section-11-building-calculator-app-using-tkinter/README.md)

## Day 5 (20/07/2020)

### Today's Progress

Learned about how to:

- Build an Database app with PostgreSQL and Python(Tkinter)
- Install Anaconda

### Links to work

[Section 12](section-12-building-database-apps-with-postgresql-&-python/README.md)

[Section 13](section-13-data-analysis-using-python/README.md)

## Day 6 (21/07/2020)

### Today's Progress

Learned about:

- Numpy, Pandas, Matplotlib library
- Analysing and making queries from data in a file

### Thoughts

Pretty interesting, thinking if I could learn more and visualize some cool data from I books or articles I read.

### Links to work

[Section 13](section-13-data-analysis-using-python/README.md)

## Day 7 (22/07/2020)

### Today's Progress

- Continued to learn about how to analyze and make queries from data in a file

- Installed Django 1 and set up for the first project 

### Thoughts

Visualizing data is fun. Hope I can learn something new or advance in the Django sections.

### Links to work

[Section 13](section-13-data-analysis-using-python/README.md)

[Section 14](section-14-make-web-applications-in-python-using-django/README.md)

## Day 8 (23/07/2020)

### Today's Progress

Learned how to:

- Set up virtual environment to run earlier version of Python and Django
- Learn to build an website with Django 1

### Thoughts

Setting up virtualenv takes lots of time today due to slow internet connection and computer :((. Learning django 1 is not really engaging as expected. 

### Links to work

[Section 14](section-14-make-web-applications-in-python-using-django/README.md)

## Day 9 (24/07/2020)

### Today's Progress

Learned about how to create forms and process forms in django 1

### Thoughts

Poor connection keep me lagging behind planned schedule. Should have been learning django 2 today.

### Links to work

[Section 14](section-14-make-web-applications-in-python-using-django/README.md)

## Day 10 (25/07/2020)

### Today's Progress

Learned about django 2

### Thoughts

Fixing PC, can't learn much. Seem not much difference from django 1, most of them are urlspatcher.  

### Links to work

[Section 15](section-15-make-web-applications-in-python-using-django-2.0/README.md)

## Day 11 (26/07/2020)

### Today's Progress

- Continued to learn about django 2

- Learn how to set up a project with REST API  

### Thoughts

Django 2 section is like some pratices for django 1. REST seems interesting, gonna learn more in the evening to catch up with my schedule.

### Links to work

[Section 15](section-15-make-web-applications-in-python-using-django-2.0/README.md)

[Section 17](section-17-building-rest-api's-with-python-and-django/README.md)

## Day 12 (27/07/2020)

### Today's Progress

- Learned some functions of Django REST API
- Learned to crawl website using python  

### Thoughts

Crawling websites is pretty interesting but might take a while to fully understanding how code works. 

### Links to work
[Section 17](section-17-building-rest-api's-with-python-and-django/README.md)
[Section 18](section-18-learn-how-to-crawl-websites-using-python-web-crawling/README.md)

## Day 13 (28/07/2020)

### Today's Progress

- Completed 2 coding challenges about tkinter.

### Thoughts

I like challenges, but 9 challenges would take a lot of time.

### Links to work

[Section 19](section-19-complex-coding-challenge)

## Day 14 (29/07/2020)

### Today's Progress

- Completed another 3 coding challenges using python.

### Thoughts

Building a tax calculator is a pretty fun task. Love the feeling when you completed challenges involving some logical thinking. 

### Links to work

[Section 19](section-19-complex-coding-challenge)

