# Section 6: Some mores types in Python

## Overview

### 47. Dictionaries

Unordered, changeable and indexed

### 48. Dictionary Function

```python
dict = {1:"one", 2:'two', "name":"Tom", "year":1990}
x = dict[1] #same as:
y = dict.get("name")
print(x,y) # one Tom
print(dict.get(5,"just put in"))
```

### 49. Tuples

Ordered and unchangeable

```python
tup = "apple", "banana", "cucumber" #can be without round brackets 
tup[0] = "lemon" #will cause error
print(tup[1])
```

### 50. List Slicing

```python
nums = [0, 1, 2, 3, 4, 5]
print(nums[::-1])
print(nums[::-2])
#Output: [5, 4, 3, 2, 1, 0]  [5, 3, 1]
```

### 51. List Comprehension

```python
nums = [1, 2, 3, 4, 5, 6]
newstr = "numbers are:{0},{1},{2}".format(nums[0], nums[1], nums[1])
print(newstr)
```

### 52. String formatting

```python
nums = [1, 2, 3, 4, 5, 6]
newstr = "numbers are:{0},{1},{2},{3}".format(nums[0], nums[1], nums[1], nums[4])
print(newstr)

a = "{x}/{y}".format(x=100, y=10)
print(a)
#Output: numbers are:1,2,2,5 | 100/10

```

### 53. String functions

```python
print(" roi den ".join(["Tao","Chuoi","Cam"]))
#Output: Tao roi den Chuoi roi den Cam
print("Hello there, goodbye there".replace("there", "World", 1))
#Output: Hello World, goodby there
print("This is a good day".startswith("This")) 
#Output: True | also .endswith()
print("fck TrumP".lower())
#Output: fck trump | also .upper()
```

### 54. Numeric functions

```python
print(min(1, 2, 3))
#Output: 1
print(abs(-8))
#Output: 8
```

### 55. Coding challenge 7

```python
class myException(Exception):
    pass
flag = True
while flag:
    try:
        n = int(input("Enter number of products:"))
        if n<5:
            raise myException("lower than 5")
        flag = False
    except myException:
        print("At least 5 products, please try again")

products = {}
for i in range(n):
    name = str(input("Enter product's name:"))
    price = float(input("Enter product's price:"))
    products.update({name:price})
print("Products with their prices:", products)
```

### 56. Coding challenge 7 solution

### 57. Coding challenge 8

```python
list = [x for x in range(100) if x%2 !=0]
print(list)
```

### 58. Coding challenge 8 solution

### 59. Notes & Summary For Section 5



