# Section 18: Learn How To Crawl Websites Using Python : Web Crawling

## Overview

### 244. Python Web Crawler Part -1

```python
from urllib.request import urlopen

html = urlsopen("http://www.wikipedia.org")
print(html.read())
```

### 245. Python Web Crawler Part -2

```python
from urllib.request import urlopen
from bs4 import BeautifulSoup

html = urlopen("http://wikipedia.org")
bsobject = BeautifulSoup(html.read(), "html.parser")
print(bsobject.title)
```

### 246. Python Web Crawler Part -3

Queue.txt and Crawled.txt. After done crawling, websites would go to crawled.txt. Avoid duplicates in 2 files.

### 247. Python Web Crawler Part -4

```python
import os

def create_project_dir(directory):
    if not os.path.exists(directory):
        print('Creating the directory' + directory)
        os.makedirs(directory)

create_project_dir('thesite')
```

### 248. Python Web Crawler Part -5

```python
import os
def create_project_dir(directory):
    if not os.path.exists(directory):
        print('Creating the directory' + directory)
        os.makedirs(directory)

def create_data_files(project_name, base_url):
    #take the project_name which is thesite, join with queue.txt
    queue = os.path.join(project_name, 'queue.txt')
    crawled = os.path.join(project_name, 'crawled.txt')
    #create files if not present
    if not os.path.isfile(queue):
        write_file(queue, base_url)
    if not os.path.isfile(crawled):
        write_file(crawled,'')
#open something and write into it
def write_file(path, data):
    with open(path, 'w') as f:
        f.write(data)
```

### 249. Python Web Crawler Part -6

```python
def append_to_file(path,data):
    with open(path,'a') as f:
        f.write(data,'\n') #print in the next line
        
def delete_file_contents(path):
    open(path, 'w').close()
```

### 250. Python Web Crawler Part -7

Files are stored in hard drive, takes long time to process => Store in ram

```python
#take a file and #replace newline with blank space
def file_to_set(file_name):
    results = set()
    with open(file_name, 'rt') as f:
        for line in f:
            results.add(line.replace('\n',''))
    return results
#add links to files
def set_to_file(links,file_name):
    with open(file_name,"w") as f:
        for l in sorted(links):
            f.write(l + '\n')
```

### 251. Python Web Crawler Part -8

```python
from html.parser import HTMLParser
from urllib import parse

class LinkFinder(HTMLParser):
    
    def __init__(self, base_url, page_url):
        super().__init__()
        self.base_url = base_url
        self.page_url = page_url
        self.links = set()
        
    def error(self, message):
        pass
        
    def handle_starttag(self, tag, attrs):
        print(tag)
```

### 252. Python Web Crawler Part -9

```python
from html.parser import HTMLParser
from urllib import parse

class LinkFinder(HTMLParser):

    def __init__(self, base_url, page_url):
        super().__init__()
        self.base_url = base_url
        self.page_url = page_url
        self.links = set()

    def error(self, message):
        pass
    
#find complete url
    def handle_starttag(self, tag, attrs):
        if tag == 'a':
            for (attribute, value) in attrs:
                if attribute == 'href'
                    url = parse.urljoin(self.base_url,value)
                    self.links.add(url)
    
    def page_links(self):
        return self.links
```

### 253. Python Web Crawler Part -10

```python
from urllib.request import urlopen
from link_finder import LinkFinder
from demo import *

class Spider:

    project_name = ''
    base_url = ''
    domain_name = ''
    queue_file = ''
    crawled_files = ''
    queue = set()
    crawled = set()
    
    def __init__(self):        
```

### 254. Python Web Crawler Part -11

```python
def __init__(self, project_name, base_url, domain_name):
    Spider.project_name = project_name
    Spider.base_url = base_url
    Spider.domain_name = domain_name
    Spider.queue_file = Spider.project_name + '/queue.txt'
    Spider.crawled_files = Spider.project_name + '/crawled.txt'
    self.boot()
    self.crawl_page('First spider', Spider.base_url)
```

### 255. Python Web Crawler Part -12

```python
@staticmethod
def boot():
    create_project_dir(Spider.project_name)
    create_data_files(Spider.project_name, Spider.base_url)
    Spider.queue = file_to_set(Spider.queue_file)python
    Spider.crawled = file_to_set(Spider.crawled_file)
```

### 256. Python Web Crawler Part -13

```python
@staticmethod
def crawl_page(thread_name, page_url):
    if page_url not in Spider.crawled:
        print(thread_name + 'Now crawling' + page_url )
        print('Queue' + str(len(Spider.queue)) + '| Crawled' + str(len(Spider.crawled))
        Spider.add_links_to_queue(Spider.gather_links(page_url))
        Spider.queue.remove(page_url)
        Spider.crawled.add(page_url)
        Spider.update_files()
```

### 257. Python Web Crawler Part -14

```python
@ staticmethod
def gather_links(page_url):
    html_string = ''
    try:
        response = urlopen(page_url)
        if 'text/html' in response.getheader('Content-Type'):
            html_bytes = response.read()
            html_string = html_bytes.decode('utf-8')
        finder = LinkFinder(Spider.base_url, page_url)
        finder.feed(html_string)
    except Exception as e:
        print(str(e))
        return set()  
    return finder.page_links()
```

### 258. Python Web Crawler Part -15

```python
@staticmethod
def add_links_to_queue(links):
    for url in links:
        if (url in Spider.queue) or (url in Spider.crawled):
            continue
        if Spider.domain_name != get_domain_name(url):
            continue
        Spider.queue.add(url)
@staticmethod
def update_files():
    set_to_file(Spider.queue, Spider.queue_file)
    set_to_file(Spider.crawled, Spider.crawled_file)
```

### 259. Python Web Crawler Part -16

```python
from urllib.parse import urlparse

def get_domain_name(url):
    try:
        results = get_sub_domain_name(url).split('.')
        return results[-2] + '.' + results[-1]
    except:
        return ''

def get_sub_domain_name(url):
    try:
        return urlparse(url).netloc
    except:
        return ''
```

### 260. Python Web Crawler Part -17

```python
import threading
from queue import Queue
from spider import Spider
from domain import *
from demo import *

PROJECT_NAME = 'thesite'
HOMEPAGE = 'http://dazzlingschoolpune.in/'
DOMAIN_NAME = get_domain_name(HOMEPAGE)
QUEUE_FILE = PROJECT_NAME + '/queue.txt'
CRAWLED_FILE = PROJECT_NAME + '/crawled.txt'
NUMBER_OF_THREADS = 8
queue = Queue()
Spider(PROJECT_NAME, HOMEPAGE, DOMAIN_NAME)
```

### 261. Python Web Crawler Part -18

```python
import threading
from queue import Queue
from spider import Spider
from domain import *
from demo import *

PROJECT_NAME = 'thesite'
HOMEPAGE = 'http://www.thptxuandinh-hanoi.edu.vn/'
DOMAIN_NAME = get_domain_name(HOMEPAGE)
QUEUE_FILE = PROJECT_NAME + '/queue.txt'
CRAWLED_FILE = PROJECT_NAME + '/crawled.txt'
NUMBER_OF_THREADS = 8
queue = Queue()
Spider(PROJECT_NAME, HOMEPAGE, DOMAIN_NAME)

def crawl():
    queued_links = file_to_set(QUEUE_FILE)
    if len(queued_links) > 0:
        print(str(len(queued_links))+' Links in the queue ')
        create_jobs()

def create_jobs():
    for link in file_to_set(QUEUE_FILE):
        queue.put(link)
        queue.join()
        crawl()
```

### 262. Python Web Crawler Part -19

Add to demo.py with encoding = 'utf-8'

```python
def create_workers():
    for _ in range(NUMBER_OF_THREADS):
        t = threading.Thread(target=work)
        t.daemon = True
        t.start()
def work():
    while True:
        url = queue.get()
        Spider.crawl_page(threading.current_thread().name, url)
        queue.task_done()

create_workers()
crawl()
```