# Section 12: Building Database Apps With PostgreSQL & Python

## Overview

How to connect Python to Postgres, to add, display, search data to a desktop app.

### 115. Introduction To Databases

Input => Python code => Output => Database

Why store data? 
- Read data
- Update/modify data
- Delete data

### 116. Introduction To PostgreSQL
Quite similar to MS SQL
### 117. Installing PostgreSQL On Windows

### 119. Creating A Database

CREATE DATABASE demo;

\c student

\l

### 120. Creating Table & Adding Data

CREATE TABLE students(name text, age int, address text);

\d 

INSERT INTO students(name, age, address) VALUES ('John', '20', 'LA');
### 121. Reading Data From Database

SELECT * FROM students WHERE age = 20;

### 123. Setting Up Virtualenv On Windows

pip install virtualenv

### 124. Installing Psycopg2

pip install psycopg

### 125. Connecting To Database With Python Code
conn = psycopg2.connect(dbname="postgres", user="postgres", password="123456", port='5432', host="localhost")
### 126. Creating Database Tables With Python

    cur = conn.cursor()
    query = '''CREATE TABLE students(name text, age int, address text);'''
    cur.execute()
    print("Database created")
    conn.commit()
    conn.close(


### 127. Adding Data To Database Tables With Python

query = '''INSERT INTO customer(NAME,AGE,ADDRESS) VALUES(%s,%s,%s)'''

### 128. User Submitted Data To Database

query = '''INSERT INTO customer(NAME,AGE,ADDRESS) VALUES(%s,%s,%s)'''

### 129. Creating App Layout

### 130. Adding Entries

entry_age = Entry(frame)
entry_age.grid(row=1, column=1)

### 131. Saving Entries To Database

```python
def get_data(name, age, address):
    conn = psycopg2.connect(dbname="postgres", user="postgres", password="123456", port='5432', host="localhost")
    cur = conn.cursor()
    query = '''INSERT INTO customer(NAME,AGE,ADDRESS) VALUES(%s,%s,%s)'''
    cur.execute(query,(name,age,address))
    print("Data inserted")
    conn.commit()
    conn.close()
    canvas = Canvas(root, height=480, width=900)
canvas.pack()

frame = Frame()
frame.place(relx=0.3, rely=0.1, relwidth=0.8)

label = Label(frame, text="Add data")
label.grid(row=0, column=1)

label = Label(frame, text="Name")
label.grid(row=1, column=0)
entry_name = Entry(frame)
entry_name.grid(row=1, column=1)

label = Label(frame, text="Age")
label.grid(row=2, column=0)
entry_age = Entry(frame)
entry_age.grid(row=2, column=1)

label = Label(frame, text="Address")
label.grid(row=3, column=0)
entry_address = Entry(frame)
entry_address.grid(row=3, column=1)

button = Button(frame, text="Add", command=lambda:get_data(entry_name.get(),entry_age.get(),entry_address.get()))
button.grid(row=4, column=1)
```

### 132. Search Functionality

```python
def search(id):
  conn = psycopg2.connect(dbname="postgres", user="postgres", password="123456", port='5432', host="localhost")
  cur = conn.cursor()
  query = '''select * from customer where id=%s'''
  cur.execute(query,(id))
  row = cur.fetchone()
  display_search(row)
  conn.commit()
  conn.close()

def display_search(row):
  listbox = Listbox(frame, width=20, height=1)
  listbox.grid(row=9, column=0)
  listbox.insert(END, row)
```

### 134. Listing All Entries

[Customer.py](customer.py)