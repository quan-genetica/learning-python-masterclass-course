# Section 8: Object Oriented Programming In Python

## Overview

Basic knowledge about class in Python

### 72. Introduction To Object Oriented Programming, Classes & Objects

### 73. Class Methods

### 74. Creating A Class & Defining Class Attributes

### 75. Instance Attributes & Constructor

### 76. Implementing Methods In Object Oriented Programming

### 77. Function Based vs OOP Way Of Writing Code

### 78. Inheritance

### 79. Multiple Inheritance

Class C(B, A):

### 80. Multi-level Inheritance

class B(A):

classC(A):

Is used more than multiple inheritance

### 81. Recursion in Python

### 82. Sets

Unordered, has unique elements.

```python
set1 = {1, 2, 3, 4, 6}
set2 = {3, 6, 7, 8}
set1.add(7)
print(set1  |  set2)
print(set1  &  set2)
print(set1  -  set2)
#{1, 2, 3, 4, 6, 7, 8}
#{3, 6, 7}
#{1, 2, 4}
```

### 83. Itertools

```python
from itertools import count
for i in count(3):
    print(i)
    if i >=20:
        break
```

### 84. Operator overloading in Python

### 85. Data hiding in Python

```python
class Myclass:
    __hiddenvar = 0
    def add(self, increment):
        self.__hiddenvar += increment
        print(self.__hiddenvar)
class1  = Myclass()
class1.add(5) #Print 5
print(class1.__hiddenvar) #Error here
```

### 86. Coding challenge 12

[CodingChallenge12](section-08-object-oriented-programming-in-python/CC12.py)

### 87. Coding challenge 12 solution

### 88. Coding challenge 13

[CodingChallenge13](section-08-object-oriented-programming-in-python/CC13.py)

### 89. Coding challenge 13 solution

### 90. Notes & Summary For Section 7

