class Computer:
    def __init__(self, ram, cpu):
        self.ram = ram
        self.cpu = cpu
    def getspec(self):
        self.ram = input("Enter ram size")
        self.cpu = input("Enter processor")
    def displayspec(self):
        print("Ram size:", self.ram,"Proceessor:", self.cpu)
class Laptop(Computer):
    def __init__(self, wgt):
        self.wgt = wgt
    def getweight(self):
        self.wgt = input("Enter weight")
    def displayweight(self):
        print("Weight is", self.wgt, "kg")
class Desktop(Computer):
    def __init__(self, disk):
        self.disk = disk
    def getdiskcapa(self):
        self.disk = input("Enter disk capacity:")
    def displaydisk(self):
        print("Disk capacity is:", self.disk)
Lap1 = Laptop('')
Desk1 = Desktop('')
Lap1.getspec()
Lap1.getweight()
Lap1.displayspec()
Lap1.displayweight()