# Section 4: Functions & Modules in Python

## Overview

### 28. Passing arguments to functions in Python 

**Parameter** (tham số)is variable in the declaration of function.

**Argument**(đối số) is the actual value of this variable that gets passed to function.

Also TIL: add ":typename" after parameter  

```python
#Define a method with two parameters
def sum(num1:int, num2:float):
   x:float =num1+num2
   print (x)
#Call the method using two arguments
sum(2, 3.5)
sum(4,5)
#Output: 5.5 / 9
```

### 29. Making function return value in Python

How to do it: assign a variable when call a function. Eg: result is the variable

```python
def add(a,b):
    c = a + b
    return c
result = add(10,20)
print(result) #output: 30
```

### 30. Passing functions as arguments in Python

```python
def add(a,b):
    return a+b
def square(c):
    return c*c
rs = square(add(2,5))
print(rs) #output: 47 
```

### 31. Modules in Python 

Modules : A set of codes like a code library that can be imported.

```python
import random
for x in range(5):
    result = random.randint(1,6)
    print(result)
#Output example: 1 2 5 4 6

def greeting(name):
	print("Hello, " + name) #save this file as mymodule.py
import mymodule:
    mymodule.greeting("Quan") #Output: Hello, Quan
```

### 32. Coding challenge 4 

### 33. Coding challenge 4 solution

```python
#Create a BMI calculator, BMI which stands for Body Mass Index can be calculated using the formula: BMI = (weight in Kg)/(Height in Meters)^2.
def BMI(w,h):
    result = w/(h*h) #also can be: w/(pow(h,2))
    print(result)
BMI(float(input("Enter your weitght:")),float(input("Enter your height:")))
```

### 34. Notes & Summary For Section 3

