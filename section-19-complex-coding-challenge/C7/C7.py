import tkinter as tk
import time as tm

def display_time():
	current_time = tm.strftime('%H:%M:%S+1000')
	clock_label['text'] = current_time
	window.after(1000,display_time) #1000 =1 second

window = tk.Tk()
window.title("Digital clock")
window.geometry('200x100')
#Label
label_hcm = tk.Label(window, text="Hanoi - GMT+7:")
label_hcm.grid(column=0, row=0)
clock_label = tk.Label(window, bg = 'white', fg='black')
clock_label.grid(row=0,column=1, sticky='s,e')
display_time()
window.mainloop()