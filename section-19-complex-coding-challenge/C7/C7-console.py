import datetime
import pytz

today=datetime.datetime.now()
today.isoformat()
print("Hanoi now is",today)

today2=today.astimezone(pytz.timezone('Europe/Amsterdam'))
today2.isoformat()
print("Amsterdam now is",today2)


today3=today.astimezone(pytz.timezone('US/Eastern'))
today3.isoformat()
print("Eastern US now is",today3)


today4=today.astimezone(pytz.timezone('Europe/Belfast'))
today4.isoformat()
print("Belfast now is",today4)


today5=today.astimezone(pytz.timezone('Asia/Tokyo'))
today5.isoformat()
print("Tokyo now is",today5)
