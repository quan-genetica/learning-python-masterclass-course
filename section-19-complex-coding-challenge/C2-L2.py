#done
from tkinter import *
import tkinter as tk

root = Tk()
root.title("Tax calculator")
root.geometry("550x400")


#Functions
def checkage():
	if int(age.get())<=60:
		return businesstype()
	else: 
		return above60()
def businesstype():
	if busi.get()=='Individual':
		return under60()
	else:
		return under60()+(float(income.get())*0.075)

def under60():
    i = float(income.get())
    if i <= 20000:
        return i 
    elif 20001<=i and i<=50000:
        return i*0.2
    elif 50001<=i and i<=100000:
        return i*0.3
    elif i>=100001:
        return i*0.4

def above60():
	j = float(income.get())

	if j <= 20000:
	    return j 
	elif 20001<=j and j<=50000:
	    return j*0.1
	elif 50001<=j and j>=100000:
	    return j*0.2
	elif i>=100001:
	    return j*0.3

def result():
	rs = checkage()
	displayarea = tk.Text(master = root, width=30,height=5)
	displayarea.grid(column=3, row= 7)
	displayarea.insert(tk.END,"Tax amount is "+ str(rs))
	

#option
busi = StringVar(root)
busi.set("Individual") 
option = OptionMenu(root, busi, "Individual", "Business")
option.grid(column=1,row=3)


#Entry
age = tk.Entry()
age.grid(column=1, row=1)

income = tk.Entry()
income.grid(column=1, row=2)


#Label
title = tk.Label(text="Enter age")
title.grid(column=0, row=1)

title = tk.Label(text="Enter salary")
title.grid(column=0, row=2)

title = tk.Label(text="Taxpayers type:")
title.grid(column=0, row=3)


#Button
button2 = tk.Button(text="Calculate tax", command=result, bg="lightyellow")
button2.grid(column=1, row=5)

root.mainloop()