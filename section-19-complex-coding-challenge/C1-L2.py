from tkinter import *
import tkinter as tk

root = Tk()
root.title("Tile Contractor")
root.geometry("550x400")


#Functions
def convert():
	lg = int(length.get())
	br = int(breadth.get())
	if lgunit.get() == "meter":
		lg  = lg*3.2808399
	else: 
		lg = lg
	if brunit.get() =="meter":
		br = br*3.2808399
	else:
		br = br
	return (lg, br)

def result():
	x, y = convert()
	dc = (float(discount.get()))/100
	a = x*y
	dced = x*y*20*dc
	p = x*y*20*(1-dc)
	displayarea = tk.Text(master = root, width=30,height=5)
	displayarea.grid(column=3, row= 7)
	displayarea.insert(tk.END,"Area is "+ str(a)+" square feet"+"\n")
	displayarea.insert(tk.END,"Total discount is $"+ str(dced)+"\n")
	displayarea.insert(tk.END,"Price is $"+ str(p))
	
#option
lgunit = StringVar(root)
lgunit.set("feet") 
option = OptionMenu(root, lgunit, "feet", "meter")
option.grid(column=2,row=1)

brunit = StringVar(root)
brunit.set("feet") 
option = OptionMenu(root, brunit, "feet", "meter")
option.grid(column=2,row=2)

#Entry
length = tk.Entry()
length.grid(column=1, row=1)

breadth = tk.Entry()
breadth.grid(column=1, row=2)

discount = tk.Entry()
discount.grid(column=1, row=3)


#Label
title = tk.Label(text="Enter length")
title.grid(column=0, row=1)

title = tk.Label(text="Enter breadth")
title.grid(column=0, row=2)

title = tk.Label(text="Enter discount")
title.grid(column=0, row=3)

title = tk.Label(text="%")
title.grid(column=2, row=3)

#Button
button1 = tk.Button(text="Calculate area and price", command=result, bg="lightblue")
button1.grid(column=1, row=5)




root.mainloop()