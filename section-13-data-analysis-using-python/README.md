# Section 13: Data Analysis Using Python

## Overview

### 134. Introduction to Data Analysis Using Python

### 135. Installing Tools For Data Analysis

### 136. Introduction to Pandas & Series

```python
from pandas import Series
se = Series([1, 3, 5, 7, 9])
se
#Out: 0 1 | 1 3 | 2 5 | 3 7 | 4 9 | dtype: int64
se[2]
#Out: 5
se2 = Series([100, 200, 300], index=['a', 'b', 'c'])
#Out: a 100 | b 200 | c 300 | dtype: int64

```

### 137. Converting Dictionaries to Series

```python
salary = {'John':3000, 'Tim':4500, 'Rob':1000}
salary
#Out: {'John':3000, 'Tim':4500, 'Rob':1000}
se3 = Series(salary)
se3
#Out: John	3000
#	  Rob   5600
#     Tim 	1000
#     dtype: int64  
se3['Rob']
#Out: 5600
```

### 138. Introduction to Data Frames

```python 
data = {'Name':['John','Tim','Rob'],
        'Age':[34,31,75],
       	'Salary':[3200,1100,4500]}
from pandas import DataFrame
frame = DataFrame(data)
frame
#Output:
```

​					![1](1.png)

### 139. Changing Column Sequence

```python 
new_frame = DataFrame(data, columns=['Name','Age','Salary'])
new_frame
new_frame['Name']
#Qut:0    John
#	 1     Tim
#	 2     Rob
#	 Name: Name, dtype: object
new_frame.loc[1] #have to use .loc[] instead of .ix[]
#Out: Name       Tim
#	  Age         31
#     Salary    1100
#     Name: 1, dtype: object
```

### 140. Changing Columns & Transposing Dataframe

```python 
new_frame = new_frame.T
new_frame
#Out:
```

​		![2](2.png)				

### 141. Reindexing Series & DataFrames

```python 
from pandas import DataFrame
data = {'Name':['John','Tim','Rob'],
        'Age':[34,31,75],
       	'Salary':[3200,1100,4500],
        'Profile':['Teacher', 'Doctor', 'Clerk']}
frame1 = DataFrame(data)
frame1 = frame1.reindex([2,0,1])

fields = ['Name','Age','Profile','Age']
frame1 = frame1.reindex(columns=fields)
frame1
#Out: 
```

![3](3.png)

### 142. Deleting Rows & Columns

```python 
frame2 = frame1.drop([2])
frame3 = frame1.drop('Salary', axis=1)
```

### 143. Arithmetic operations on dataframe and series

```python
from pandas import Series
se1 = Series([1,2,3,4,5])
se2 = Series([10,20,30,40,50])
se1 + se2
"""
Out:
0    11
1    22
2    33
3    44
4    55
dtype: int64
"""
from pandas import DataFrame
data1 = {'Speed':[100,103,105],
        'Temp':[34,21,40],
        'Humidity':[60,80,90]}
frame1 = DataFrame(data1)
data2 = {'Speed':[100,103,107],
        'Temp':[34,21,70],
        'Humidity':[60,80,80]}
frame2 = DataFrame(data1)
frame1 + frame2

```

|      | Speed | Temp | Humidity |
| ---: | ----: | ---: | -------: |
|    0 |   100 |   34 |       60 |
|    1 |   103 |   21 |       80 |
|    2 |   105 |   40 |       90 |

### 144. Arithmetic operations in between dataframe and series

In [11]:

```python
series3 = Series([100,140,130], index=['Speed','Temp','Humidity'])
series3
```

Out[11]:

```python
Speed       100
Temp        140
Humidity    130
dtype: int64
```

In [10]:

```python
frame2-series3
```

Out[10]:

|      | Speed | Temp | Humidity |
| ---: | ----: | ---: | -------: |
|    0 |     0 | -106 |      -70 |
|    1 |     3 | -119 |      -50 |
|    2 |     5 | -100 |      -40 |

### 145. Sorting series and dataframes

```python
ser = Series([3,8,6,4,1,9], index=[2,4,8,4,1,5])
ser
ser.sort_index()
'''
1    1
2    3
4    8
4    4
5    9
8    6
dtype: int64
'''
frame2.sort_index(axis=1, ascending=False) #sort the id column from highest->lowest
```

### 146. Sorting according to values

```python
ser = Series([3,8,6,4], index=['sp','temp','humid','date'])
ser.sort_values()
'''
sp       3
date     4
humid    6
temp     8
dtype: int64
'''
frame2.sort_values(by="Humidity")
#Out:
```

|      | Speed | Temp | Humidity |
| ---: | ----: | ---: | -------: |
|    0 |   100 |   34 |       60 |
|    1 |   103 |   21 |       80 |
|    2 |   105 |   40 |       90 |

### 147. Handling duplicate values

Out[22]:

```python
ser.index.is_unique
True
```

### 148. Calculating sum, max & min values

```python
frame2.sum()
'''
Speed       308
Temp         95
Humidity    230
dtype: int64
'''
```

### 149. Dropping nan values

```python
ser.dropna() #also for frame

ser.fillna(10)
```

### 150. Loading data from a file

```python
import pandas
data_frame = pandas.read_csv("filename.csv") #excel json
```

### 151. Analyzing file data

```python
data_frame.sum(numeric_only=True)
```

### 152. Creating a Numpy Array

```python
import numpy as np
a = np.array([1,2,3,4,5,5])
print(a)
#[1 2 3 4 5 5]
a.reshape(2,3)
#Out:array([[1, 2, 3], [4, 5, 5]])
```

### 153. 19 Another way to create an array

```python
import numpy as np
a = np.arange(24)
print(a)
#Out: [ 0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23]
b=a.reshape(2,4,3) #2 big groups, each has 4 arrays, each has 3 ele
print(b)
#Out:
''' 
[[[ 0  1  2]
  [ 3  4  5]
  [ 6  7  8]
  [ 9 10 11]]

 [[12 13 14]
  [15 16 17]
  [18 19 20]
  [21 22 23]]]
 '''
x = np.zeros(5, dtype=np.int)
print(x)

#[0 0 0 0 0]
```

### 154. Logspace & Linspace

```python
import numpy as np
a = np.linspace(10,20,5, endpoint=False)
#from 10 to 20, divide into 5 ele except 20
print(a)
#[10. 12. 14. 16. 18.]

a = np.logspace(1,10,num=10, base=2)#10 numbers, from 2^1 to 2^10
print(a)
#[   2.    4.    8.   16.   32.   64.  128.  256.  512. 1024.]
```

### 155. Slicing a Numpy Array

```python
import numpy as np
a = np.arange(10)
print(a)
[0 1 2 3 4 5 6 7 8 9]
```

```python
s = slice(2, 7, 2) #from 2 to 7, seperate by 2
print(a[s])
[2 4 6]
```

```python
b = a[2:5:2]
print(b)
[2 4]
```

```python
print(a[2:5])
[2 3 4]
```

```python
a = np.array([[1,2,3],[4,5,6],[7,8,9]])
print(a)
[[1 2 3]
 [4 5 6]
 [7 8 9]]
```

```python
print(a[:2])
[[1 2 3]
 [4 5 6]]
```

### 156. Advanced indexing and slicing techniques

```python
#a as above
b = a[[0,1,2],[0,1,0]] 
print(b)
[1 5 7]
```

In [62]:

```python
c = np.array([[1,2,3],[4,5,6],[7,8,9],[10,11,12]])
print(c)
[[ 1  2  3]
 [ 4  5  6]
 [ 7  8  9]
 [10 11 12]]
```

```python
row = np.array([[0,1],[2,2]])
cols = np.array([[0,2],[0,1]]) 
d = c[row,cols] # =>row-col: 00, 12, 20, 21
print(d)
[[1 6]
 [7 8]]

print(c[c>3])
[ 4  5  6  7  8  9 10 11 12]
```

### 157. Broadcasting

```python
import numpy as np
a = np.array([1,2,3,4])
b = np.array([10,20,30,40])
c = a*b
print(c)
[ 10  40  90 160]

a = np.array([[1,2,3],[4,5,6],[7,8,9]])
b = np.array([1,2,3])
print(a)
print(b)
[[1 2 3]
 [4 5 6]
 [7 8 9]]
[1 2 3]

print(a+b)
[[ 2  4  6]
 [ 5  7  9]
 [ 8 10 12]]
```

### 158. Iterating using nditer

```python
a = np.arange(0,60,5)
a = a.reshape(3,4)
print(a)
'''[[ 0  5 10 15]
 [20 25 30 35]
 [40 45 50 55]]'''
for x in np.nditer(a):
    print(x)
'''
0
5
10
15
20
25
30
35
40
45
50
55
'''
```

### 159. Plotting data using Matplotlib



```python
import numpy as np
from matplotlib import pyplot as plt
x = np.arange(1,11)
y = 2 * x + 5
plt.title("This is the title")
plt.xlabel("This is x label")
plt.ylabel("This is y label")
plt.plot(x,y)
plt.show()
```

### 160. Analysing Supermarket Sales Data Part 1: Reading CSV File

```python
import pandas as pd
sales = pd.read_csv('sales.csv')
```

### 161. Analysing Supermarket Sales Data Part 2: Switching Up The Theme

### 162. Analysing Supermarket Sales Data Part 3: Accessing Different Parts Of Data

```python
sales.head(10)

sales['Category']

sales['Category'].unique()

sales.iloc[200]
```

### 163. Analysing Supermarket Sales Data Part 4: Selecting Rows On A Condition

```python
sales[sales['Gender']=='Male'].head(10)
sales[sales['Total']>100]
```

### 164. Analysing Supermarket Sales Data Part 5: Queries To Find Conditional Data

```python
sales.query('100<Total<200' and 'City=="NewYork"')
```

### 165. Analysing Supermarket Sales Data Part 6: Sum, Max, Min & Average

```python
sales.sum()
#Out:
Invoice ID                                         494493053341
Date          1/25/20183/19/20182/25/20181/22/20182/18/20183...
Time          16:4616:4813:3313:3815:3117:5513:2113:2415:331...
Gender        MaleFemaleMaleFemaleFemaleMaleMaleMaleMaleFema...
Location      BrookfieldWater towerWater towerPark lanePark ...
City          NewYorkChicagoChicagoDallasDallasNewYorkChicag...
Member        YesYesNoYesNoYesNoNoYesNoNoNoNoYesNoYesYesYesY...
Category      GroceriesFashionClothingSportingBooksClothingC...
Price                                                     51112
Quantity                                                   4059
Total                                                    207788
Payment       CashCardCashGpayCashGpayCashCashCardCashGpayCa...
Rating                                                     2972
dtype: object
    
sales.sum()['Total']
sales.mean()
sales['Total'].max()
```

### 166. Analysing Supermarket Sales Data Part 7: Using GroupBy To Group Data By Location

```python
sales.groupby('City').sum() #sort by city, show all
City	Invoice ID	   Price   Quantity	Total	Rating		
Chicago	164244758326	16152	1307	65215	1023
Dallas	160721744770	17289	1344	70432	1003
NewYork	169526550245	17671	1408	72141	946

sales.groupby('Date').sum()['Total'] #sort by date, show Total
```

### 167. Analysing Supermarket Sales Data Part 8: Finding Market Share

- Which location has highest and lowest sales?

  ```python
  import matplotlib.pyplot as plt
  plt.bar(location, sales.groupby('Location').sum()['Total'])
  plt.show()
  ```

  ​		![BarChart](bar.png)

- Represent the sales on a bar chart, also show the market share for each location using a pie chart

  ```python
  plt.pie(sales.groupby('Location').sum()['Total'], labels=location, autopct='%1.1f%%')
  plt
  ```

  ​	![PieChart](pie.png)

### 168. Analysing Supermarket Sales Data Part 9: Classifying Shoppers

Which location has more female customers and which location has more male

```python
location_sales = sales.groupby(['Gender', 'Location']).count()['Invoice ID'  ]
unstacked_sales = location_sales.unstack(level=0)
unstacked_sales.plot(kind='bar')
```

![Gender-Location](genderlocation.png)	

### 169. Analysing Supermarket Sales Data Part 10: Analysing Memberships & Ratings

Which branch has highest rating and which has the lowest?

```python
rating = sales.groupby('Location').mean()['Rating']
rating.plot(kind = 'bar')
plt.show()
```

![Rating](rating.png)

### 170. Analysing Supermarket Sales Data Part 11: Answering Multiple Queries

- Which city has more females shopping?

  ```python
  gc = sales.groupby(['Gender','City',]).count()['Invoice ID']
  gc.unstack(level=0).plot(kind='bar')
  plt.show()
  ```

- Who spends more men or women?

  ```python
  spend = sales.groupby('Gender').sum()['Total']
  spend.plot(kind='bar')
  ```

- Which type of customer spends more member or a non member?

  ```python
  mem = sales.groupby('Member').sum()['Total']
  mem.plot(kind='bar')
  ```

- Which product line sells more?

  ```python
  cat_sales = sales.groupby('Category').count()['Rating']
  cat_sales = cat_sales.plot(kind='bar')
  ```

- Which product line is popular among men vs women?

  ```python
   cat_sales = sales.groupby(['Gender','Category']).count()['Rating']
  cat_sales.unstack(level=0).plot(kind='bar')
  ```

### 171. Analysing Supermarket Sales Data Part 12: Classifying Sales By Day

What days of the month make most sales?

```python
pd.to_datetime(sales['Date']).dt.month
sales['Day'] = pd.to_datetime(sales['Date']).dt.day
day_sales = sales.groupby('Day').sum()['Total']
day_sales.plot()
```

​	![daysales](daysales.png)

### 172. Analysing Supermarket Sales Data Part 13: Classifying Sales By Month

Which months make the most sales?

```python
sales['Month'] = pd.to_datetime(sales['Date']).dt.month
sbm = sales.groupby('Month').sum()['Total']
sbm.plot(kind='bar')
plt.grid()
plt.show()
```

​	![Salesbymonth](sbm.png)

### 173. Analysing Supermarket Sales Data Part 14: Classifying Sales By Hour

```python
sales['Hour'] = pd.to_datetime(sales['Time']).dt.hour
sbh = sales.groupby('Hour').sum()['Total']
sbh.plot()
plt.grid()
plt.show()
```

​	![Salesbyhour](sbh.png)

### 174. Analysing Supermarket Sales Data Part 15: Classifying Payment Types With Hour

```python
sales.groupby(['Payment','Hour']).count()['Invoice ID'].unstack(level=0).plot(kind='bar')
```

​	![PaymentHour](ph.png)

