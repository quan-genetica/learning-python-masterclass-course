# Section 7: Functional Programming in Python

## Overview

### 60. Functional programming

Function in python is an object, so it can be passed to another function:

```python
def add(x):
    return(x+10)
def once(func, arg):
    return(func(arg))

def twice(func, *arg):
    return(func(func(*arg)))

print(once(add,10))
print(twice(add,10))
#Output: 20 | 30 
```

### 61. Lambdas in Python

 Lambda functions are mainly used in combination with the functions filter(), map() and reduce().

```python
print((lambda x: x**2)(5)) #Output: 25
f = lambda x, y : x + y
f(1,1)
#2
```

### 62. Map in Python

map(function, iterable)    | *iretable means lists, tuples, dictionaries...

```python
def multiply(x):
    return x*2
list1 = [1, 2, 3, 4, 5]
rs = list(map(multiply, list1))
print(rs)
rs1 = list(map(lambda x: x*2, list1))
print(rs1)
#Same output: [2, 4, 6, 8, 10]
```

### 63. Filters in Python

```python
list1 = [3, 4, 5, 13, 75, 33]
rs = list(filter(lambda x: x%3==0, list1))
print(rs)
```

### 64. Generators in Python

```python
def func():
	counter = 0
	while counter<5:
		yield counter
		counter += 1
for x in func():
	print(x)
def evennum(x):
    for i in range(x):
        if i % 2 == 0:
            yield i
print(list(evennum(8))) # [0, 2, 4, 6]
```

### 65. Coding challenge 9

```python
def sd_disc(p):
    return(p*0.9)
def add_disc(p1):
    return(p1*0.95)

print(add_disc(sd_disc(100)))
#Output: 100*09*0.95= 85.5
```

### 66. Coding challenge 9 solution

### 67. Coding challenge 10

```python
result = (lambda x: x*(x+5)**2)(5)
print(result) #500
```

### 68. Coding challenge 10 solution

### 69. Coding challenge 11

```python
list1 = [10, 20, 25, 40]
discount = list(map(lambda x: x*0.9, list1))
print(discount)
```

### 70. Coding challenge 11 solution

### 71. Notes & Summary For Section 6



