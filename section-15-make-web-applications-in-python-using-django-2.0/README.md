# Secion 15: Make Web Applications With Python Using Django 2.0

### 204. Using virtual environment

```powershell
easy_install virtualenv
virtualenv pj2
pip install django==2.0
```

 ### 205. Django 2 Lecture 1 Creating a project

```powershell
django-admin startproject photop
```

### 206. Django 2 Lecture 2 Creating An App

```shell
django-admin startapp photoapp
```

### 207. Django 2 Lecture 3 Overview of an app
### 208. Django 2 Lecture 4 Creating a View

```python
def index(request):
	return HttpResponse("<h>This is the homepage</h>")

#urls.py in photo
from django.contrib import admin
from django.urls import include, path
urlpatterns = [
    path('admin/', admin.site.urls),
    path('phots/', include('photoapp.urls')),
]
#urls.py for photopapp
from django.contrib import admin
from django.urls import path
from 
urlpatterns = [
	path('', views.index, name='index'),
]
```

### 209. Django 2 Lecture 5 Applying Migrations

```python
py manage.py makemigrations photoapp
py manage.py sqlmigrate photoapp 0001
py manage.py migrate
```

### 210. Django 2 Lecture 6 Creating Models
### 211. Django 2 Lecture 7 Creating Database Tables

```
#in install apps
'photoapp.apps.PhotoappConfig'
```

### 212. Django 2 Lecture 8 Adding Data To Database

```powershell
>>>from photoapp.models import Photo
>>> Photo.objects.all()
<QuerySet []>
>>> a = Photo()
>>> a.name = "Quan"
>>> a.creator = "Quanmedia"
>>> a.price="1000"
>>> a.save()
```

### 213. Django 2 Lecture 9 Filtering Results

Same as django 1

### 214. Django 2 Lecture 10 Creating A Super User

Same as django 1

```python
from django.contrib import admin
from .models import Photo
# Register your models here.
admin.site.register(Photo)
```

### 215. Django 2 Lecture 11 Creating Another View

```python
path('<int:photo_id>/', views.detail, name='detail'),

def detail(request, photo_id):
	return HttpResponse("This is the page for photo:" + str(photo_id))
```

### 216. Django 2 Lecture 12 Connecting To The Database

```python
from .models import Photo
def index(request):
	
	all_photos = Photo.objects.all()
	html = ''
	for photo in all_photos:
		url = '/photos/' + str(photo.id) + '/'
		html += '<a href="' + url + '">' + str(photo.name) + '</a><br>'	
	return HttpResponse(html)
```

### 217. Django 2 Lecture 13 Creating Templates

```jinja2
<ul>
	{% for photo in all_photos %}
		<li>
			<a href="/photos/{{photo.id}}">{{photo.name}}</a>
		</li>
	{% endfor %}
</ul>
```

### 218. Django 2 Lecture 14 Using Render

```python
def index(request):
all_photos = Photo.objects.all()
context = {'all_photos':all_photos}
return render(request,'photoapp/index.html', context)
```
### 219. Django 2 Lecture 15 Raising 404 Error

```python
def detail(request, photo_id):
	try: 
		photo = Photo.objects.get(id=photo_id)
	except Photo.DoesNotExist:
		raise Http404('Photo not found')
	return render(request, 'photoapp/detail.html',{'photo':photo})
```

### 220. Django 2 Lecture 16 Designing The Detail View

```jinja2
<h1>{{photo.name}}</h1>
<h2>{{photo.creator}}</h2>
<h3>{{photo.price}}</h3>
```

### 221. Django 2 Lecture 17 Removing Hard Coded URLs

```jinja2
<a href="{% url 'detail' photo.id %}">
```

### 222. Django 2 Lecture 18 Using Namespaces

```jinja2
<a href="{% url 'photoapp:detail' photo.id %}">{{photo.name}}</a>
```

```python
#in urls.py
app_name = 'photoapp'
```

### 223. Django 2 Lecture 19 Using Static Files

```python
{% load static %}
{% for photo in all_photos %}
    <li>
        <a href="{% url 'photoapp:detail' photo.id %}">{{photo.name}}</a>
    </li>
{% endfor %}
```

### 224. Django 2 Lecture 20 Designing Navbar

```jinja2
#add bootstrap
<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<a class="navbar-brand" href="{% url 'photoapp:index' %}">Photo Place</a>			
		</div>
</nav>
```

### 225. Django 2 Lecture 21 Navbar Touchup

<div class="collapse navbar-collapse">
		<ul class="nav navbar-nav">
			<li class="">
				<a href="{% url 'photoapp:index'}">
					<span class="glyphicon glyphicon-camera" area-hidden="true">&nbsp; Images</span>
				</a>
			</li>
		</ul>
		</div>