# Section 10: Create GUI Apps In Python Using Tkinter

## Overview

### 99. Tkinter : Hello world program

```python
from tkinter import *

root = Tk() #a class in tkinter

label1 = Label(root, text="Hello World")
label1.pack()

root.mainloop()
```

### 100. Tkinter: Using frames

```python
from tkinter import *

root = Tk() #a class in tkinter

newframe = Frame(root)
newframe.pack()

otherframe = Frame(root)
otherframe.pack(side=BOTTOM)

button1 = Button(newframe, text = "Click Here", fg="Red")
button2 = Button(otherframe, text = "Click Here", fg="Blue")

button1.pack()
button2.pack()

root.mainloop()
```

### 101. Tkinter: Grid layout

```python
from tkinter import *

root = Tk() #a class in tkinter

label1 = Label(root, text="Firstname")
label2 = Label(root, text="Lastname")

entry1 = Entry(root)
entry2 = Entry(root)

label1.grid(row=0, column=0)
label2.grid(row=1, column=0)

entry1.grid(row=0, column=1)
entry2.grid(row=1, column=1)

root.mainloop()
```

### 102. Tkinter: Self adjusting widget

```python
from tkinter import *

root = Tk() #a class in tkinter

label1 = Label(root, text="First", bg="Red", fg="White")
label1.pack(fill=X)

label2 = Label(root, text="Second", bg="Blue", fg="White")
label2.pack(side=LEFT, fill=Y)
root.mainloop()
```

### 103. Tkinter: Handling button clicks

```python
def dosmt():
    print("You clicked the button")
button1 = Button(root, text="Click Here", command=dosmt)
button1.pack()
```

### 104. Tkinter: Using classes

```python
from tkinter import *

class MyButton:

    def __init__(self, rootone):
        frame = Frame(rootone)
        frame.pack()

        self.printbutton = Button(frame, text="Click Here", command=self.printmsg)
        self.printbutton.pack()
        self.quitbutton = Button(frame, text="Exit", command=frame.quit)
        self.quitbutton.pack(side=LEFT)
    def printmsg(self):
        print("Button CLicked")

root = Tk()
b = MyButton(root)
root.mainloop()
```

### 105. Tkinter: Using drop downs

```python
from tkinter import *
def function1():
    print("Menu item clicked")

root = Tk()

mymenu = Menu(root)
root.config(menu=mymenu)

submenu = Menu(mymenu)
mymenu.add_cascade(label="File", menu=submenu)
submenu.add_command(label="Project", command=function1)
submenu.add_command(label="Save", command=function1)
submenu.add_separator()
submenu.add_command(label="Exit", command=function1)

newmenu = Menu(mymenu)
mymenu.add_cascade(label="Edit", menu=newmenu)
newmenu.add_command(label="Undo", command=function1)

root.mainloop()
```

### 106. Tkinter: Toolbar

```python
toolbar = Frame(root, bg="Teal")
insertbutton = Button(toolbar, text="Insert Files", command=function1)
insertbutton.pack(side=LEFT, padx=2, pady=3)

printbutton = Button(toolbar, text="Print", command=function1)
printbutton.pack(side=LEFT, padx=2, pady=3)

toolbar.pack(side=TOP, fill=X)
```

### 107. Tkinter: Making Status Bar

```python
status = Label(root, text="This is the status", bd=1, relief=SUNKEN, anchor=W)
#border, side West
status.pack(side=BOTTOM, fill=X)
```

### 108. Tkinter: Message box

```python
import tkinter.messagebox
tkinter.messagebox.showinfo("HI MARS")
response = tkinter.messagebox.askquestion("Question1","Do you like cat")

if response == "yes":
    print("So bad, I'm a dog person")
```

### 109. Tkinter Drawing

```python
canvas = Canvas(root, width=200, height=100)
canvas.pack()
newline = canvas.create_line(100,150,200,0)
otherline = canvas.create_line(10,20,30,40, fill="blue")
```

