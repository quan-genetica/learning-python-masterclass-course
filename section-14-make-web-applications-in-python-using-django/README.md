# Section 14: Make Web Applications In Python Using Django

## Overview

### 175. Django Tutorial 1: Installing Django on Windows

```shell
easy_install django 1.11.2
```

### 176. Installing Django For MAC Users

### 177. Django tutorial 2: Creating Our First Django Project

```shell
djano-admin startproject mysite
```

### 178. Django tutorial 3: Creating Our Own App

```shell
django-admin startproject ecom
cd mysite
py manage.py runserver
```

### 179. Django tutorial 4: Overview Of an App in Django

###  180. Django tutorial 5: Creating Our Own Views in Django

```python
def index(request):
	return HttpResponse("<h>This is the homepage</h>")


from django.contrib import admin
from . import views
urlpatterns = [
    url(r'^$', views.index),
]

from django.conf.url import include, url
urlpatterns = [
    url(r'^books/', include('books.urls')),
]
```

### 181. Django tutorial 6: Applying Migrations

```python
#add to database in settings.py
'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
#shell
py manage.py migrate
```

### 182. Django tutorial 7: Creating Books Table in Django

```python
# in models.py
from django.db import models

class Book(models.Model):
    name = models.CharField(max_length=100)
    author = models.CharField(max_length=100)
   	price = models.CharField(max_length=100)
	btype = models.CharField(max_length=100)
```

### 183. Django tutorial 8: Final Steps in Creating Table

```python
#add to INSTALLED_APPS in settings.py
	'books.apps.BooksConfig'
```

```shell
#shell
py manage.py makemigrations books
py manage.py sqlmigrate books 0001
py manage.py migrate
```

### 184. Django tutorial 9: Adding Data to the Database Tables

```powershell
py manage.py shell
from books.models import Book
>>> Book.objects.all()
<QuerySet []>
a = Book()
>>> a.name = "Life"
>>> a.author="ABC"
>>> a.price="10"
>>> a.btype="Business"
>>> a.save()
>>> a.name
'Life'
>>> a.author
'ABC'
```

### 185. Django tutorial 10: Filtering the Results

```python
#add to Book class:
def __str__(self):
	return self.name + '-' + self.author
```

```powershell
>>> b=Book()
>>> b.name="Success"
>>> b.author="XYZ"
>>> b.price="30"
>>> b.btype="fiction"
>>> b.save()
>>> a.id
1
>>> b.id
2
>>> from books.models import Book
>>> Book.objects.all()
<QuerySet [<Book: Life-ABC>, <Book: Success-XYZ>]>
>>> Book.objects.filter(id=1)
<QuerySet [<Book: Life-ABC>]>
```

### 186. Django tutorial 11: Admin Pannel in Django

```powershell
py manage.py createsuperuser
```

### 187. Django tutorial 12: Creating Another View in Django

```python
#in views.py
def detail(request, book_id):
    return HttpResponse('<h2> Details for Book ID:' +str(book_id) + "</h2>")
#in urls.py
url(r'^(?P<book_id>[0-9]+)$', views.detail, name='detail'),
```

### 188. Django tutorial 13: Connecting to the Database

```python
def index(request):
    all_books = Book.objects.all()
    html = ''
    for book in all_books:
        url = '/books/' + str(book.id) + '/'
        html += '<a href="' + url + '">' + str(book.name) + '</a><br>'
    return HttpResponse(html)
```

### 189. Django tutorial 14: Creating Templates

```python
#templates/books/index.html
<ul>
    {% for book in all_books %}
        <li><a href="/books/{{book.id}}">{{book.name}}</a></li>
    {% endfor %}
</ul>
```

```python
def index(request):
    all_books = Book.objects.all()
    template = loader.get_template('books/index.html')
    context = {'all_books':all_books}
    return HttpResponse(template.render(context, request))
```

### 190. Django tutorial 15: Rendering Templates

```python
from django.shortcuts import render
def index(request):
    all_books = Book.objects.all()
    context = {'all_books':all_books}
    return render(request, 'books/index.html', context)
```

### 191. Django tutorial 16: Raising a 404 Error

```python
from django.shortcuts import Http404
def detail(request, book_id):
    try:
        book = Book.objects.get(id=book_id)
    except Book.DoesNotExist:
        raise Http404('This book does not exist')
    return render(request, 'books/detail.html', {'book':book})
```

### 192. Django tutorial 17: Designing The Detail View

```html
#in detail.html
<h1>{{book.name}}</h1>
<h4>{{book.author}}</h4>
```

### 193. Django tutorial 18: Removing The Hardcoded Urls

```python
<a href="{% url 'detail'  book.id %}">
```

### 194. Django tutorial 19: Namespaces in Django

When you have many apps, problems may occur when files are called with same name, then you would want to name each of them by:

```python
#in urls.py
app_name = 'books'
#in detail.html
'books:detail'
```

### 195. Django tutorial 20: Using Static Files In Django

```python
<html>
{% load static %}
    <head>
        <link rel="stylesheet" type="text/css" href="{% static 'books/style.css'%}">
    </head>
    <body>
        <ul>
            {% for book in all_books %}
            <li><a href="{% url 'books:detail' book.id %}">{{book.name}}</a></li>
            {% endfor %}
        </ul>
    </body>
</html>

#style.css
body{
    background: white url("back.jpg")
}
```

### 196. Django tutorial 21: Creating Our Navigation Bar

```html
#insert bootstrap code
<nav clas="navbar navbar-inverse">
    <a class="navbar-brand" href="{% url 'book:index' %}">The Bookstore</a>
</nav>
<ul class="nav navbar-nav">
                        <li class="">
                            <a href="{% url 'books:index' %}">
                                <span class="glyphicon glyphicon-book" aria-hidden="true"></span>&nbsp;Books
                            </a>
                        </li>
                    </ul>
```

### 197. Django tutorial 22: Navigation Bar Touchup

```html
<ul class="nav navbar-nav navbar-right">
    <li class="">
        <a href="{% url 'books:index' %}">
            <span class="glyphicon glyphicon-book" aria-hidden="true"></span>&nbsp;Add Book
        </a>
    </li>
</ul>
<ul class="nav navbar-nav navbar-right">
     <li class="">
          <a href="{% url 'books:index' %}">
              <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>&nbsp;Logout
          </a>
     </li>
</ul>
```

### 198. Django tutorial 23: Using Base Templates

```python
#in base.html
bootstrap, css..
<body>
 	{% block body %}
	{% endblock %}
</body>
#in specific pages template
{% extends 'books/base.html' %}
{% block body %}
....
{% endblock %}
```

### 199. Django tutorial 24: Generic Views In Django

Easier to code and modify.

```python
from django.views import generic
from .models import Book

class IndexView(generic.ListView):
    template_name = 'book/index.html'
    
    def get_queryset(self):
        return Book.objects.all()
   
class DetailView(generic.DetailView):
    model = Book
    template_name = 'books/detail.html'
    
#in urls.py
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    #/books/2
    url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
]	
```

### 200. 

```python
# index.html
<img src="{{book.book_image}}" alt="">
#style.css
body {
    //background: whit url("images/back.jpg");
}
.jumbotron{
    background: white url("images/yellow.jpg");
}
.jumbotron-special{
    background: white url("images/purple.jpg");
}
.jumbotron-special h2{
    color:white;
}

            
```

### 201. Django Tutorial 26: Form To Add books

```python
#in models.py
from django.core.urlresolvers import reverse
def get_absolute_url(self):
        return reverse('books:detail', kwargs={'pk:self.pk'})
#in views.py
class BookCreate(CreateView):
    model = Book
    field = ['name', 'author', 'price', 'btype', 'book_image']
    
#in urls.py
url(r'books/add/$', views.BookCreate.as_view(), name='book_add'),
```

### 202.Django tutorial 27 Form part 2

```python
 <form class="form-horizontal" action="" method="post" enctype="multipart/form-data">
 {% csrf_token %}
  	{% include 'books/form-template.html' %}
        <div class="form-group">
             <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-sucess">Submit</button>
             </div>
        </div>
  </form>
```

