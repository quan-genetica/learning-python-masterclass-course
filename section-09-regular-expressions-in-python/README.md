# Section 9: Regular Expressions In Python

## Overview

### 91. Regular expressions in Python

```python
import re
pattern = r"tap"
if re.match(pattern, "taptap"):
    print("Match found")
if re.search(pattern, "watertapwater"):
    print("Also found")
else:
    print("No match found")
```

### 92. Search & find all

```python

print(re.findall(pattern, "watertapwater"))
#Match found
#Also found
#['tap']
```

### 93. Find & replace

```python
import re
string = "My name is Quan"
pattern = r"Quan"
newstr = re.sub(pattern, "Tom", string)
print(newstr)
#My name is Tom
```

### 94. The dot metacharacter

```python
import re
pattern = r"gr....r" #any character in .
if re.match(pattern, "graaaar"):
    print("found!") 
#found!
```

### 95. Caret & dollar metacharacter

```python
import re
# ^bl: starts with bl
# nk$: ends with nk
pattern = r"^bl.nk$" 
if re.match(pattern, "blink"):
    print("found!")
#found!
```

### 96. Character class

```python
import re
pattern = r"[A-Z][0-9][A-Z]"
if re.match(pattern, "T3B"):
    print("found!")
#found!
```

### 97. Star metacharacter

### 98. Group

```python
import re
pattern = r"eggs(bacon)*bread" 
#bacon multiply 0, 1 or any times in a string
if re.match(pattern, "eggsbread"):
    print("found!")
#found!
```

 

