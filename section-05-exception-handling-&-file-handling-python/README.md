# Section 5: Exception Handling & File Handling Python

## Overview

### 35. Errors & exceptions in Python 

### 36. Exception handling in Python

```python
try:
    a = 2
    b = 0
    print(a/b)
except ZeroDivisionError:
    print("There is a divide by zero error!")
#To warn user about the error
```

### 37. Finally block

Finally can't be alone, must be in a try-except block.

```python
try:
    a = 2
    b = 0
    print(a/b)
except ZeroDivisionError:
    print("There is a divide by zero error!")
finally:
    print('gonna be printed no matter what')
```

### 38. File handling

```python
file = open("demo.txt", 'w')
#gonna create a file in the same folder if not exist yet 
file.close()
```

### 39. Reading data from file

```python
file = open("demo.txt", 'r')
content = file.read()
print(content)
file.close()
```

### 40. Adding data to the file

```python
file = open("demo.txt", 'w')
file.write("this is written in the file") #delete all previous content
file.close()
```

### 41. Appending to a file

```python
file = open("demo.txt", 'a') #to append
```

### 42. Coding challenge 5

Write a function which would divide two numbers, design the function in a manner that it handles the divide by zero exception.

```python
def divide(a,b):
    try:
        return(a/b)
    except ZeroDivisionError:
        print("Can't divide by 0")
        return 0
quotient = divide(float(input("Enter dividend:")),float(input("Enter divisor:")))
print(quotient)
```

### 43. Coding challenge 5 solution

### 44. Coding challenge 6

```python
 #Write data to the file
file = open("demo.txt", "w")
file.write("This is some random data")
file.close()

#Read its data
file = open("demo.txt", "r")
print(file.read()) #display only the first write
file.close()

# add some more content
file = open("demo.txt", "a")
file.write(" additional content") 
file.close()

```

### 45. Coding challenge 6 solution

### 46. Notes & Summary For Section 4



